<?php

namespace Engine\Tasks;

use Engine\Exceptions\ProductCardImageGrabber\NotExistException;
use Engine\Exceptions\ProductCardImageGrabber\WrongExtensionException;
use Engine\Models\ProductCard;
use Engine\Models\ProductCardLocalImage;
use Engine\Services\ProductCard\ProductCardLocalImageService;
use Phalcon\Db;
use Phalcon\Db\Column;

/**
 * Class ProductCardImageGrabberTask
 * @package Engine\Tasks
 */
class ProductCardImageGrabberTask extends AbstractMonitoredTask
{
    public const PRODUCT_CARD_BATCH_LIMIT = 100;

    public const WAIT_LIMIT = '3 HOUR';

    /**
     * @var int
     */
    private $productCardBatchLimit = self::PRODUCT_CARD_BATCH_LIMIT;

    /**
     * @var int
     */
    private $bulkTimesLimit;

    /**
     * @var ProductCardLocalImage[]|null
     */
    private $productCardsLocalImagesBatch = [];

    /**
     * @var ProductCardLocalImageService
     */
    private $productCardLocalImageService;

    /**
     * {@inheritdoc}
     */
    public function initialize(): void
    {
//        $this->info('Registering onShutdown() function...');
//        \register_shutdown_function([$this, 'onShutdown']);
//        $this->info('OK');

        $this->info('Running parent::initialize()...', true);
        parent::initialize();
        $this->info('OK');

        /** @var ProductCardLocalImageService $productCardLocalImageService */
        $this->productCardLocalImageService = $this->di->get('ProductCardLocalImageService');
    }

    /**
     * php app/cli.php ProductCardImageGrabber runStreams 4
     *
     * 4 - number of stream (required)
     *
     * @param array $params
     * @throws \Exception
     */
    public function runStreamsAction(array $params): void
    {
        if ('dev' === APP_ENV) {
            if (!isset($params[0]) || !is_numeric($params[0]) || $params[0] < 1) {
                throw new \Exception('Wrong params');
            }

            $cnt = (int)$params[0];

            for ($i = 0; $i < $cnt; $i++) {
                $this->info('Run process #' . $i . '...');
//                exec('php app/cli.php ProductCardImageGrabber run 1 100 > /dev/null 2>&1 &');
                exec('php app/cli.php ProductCardImageGrabber run 1 20');
            }
        }
    }

    /**
     * php app/cli.php ProductCardImageGrabber run
     * php app/cli.php ProductCardImageGrabber run 1 100
     *
     * 1 - count of batches (optional)
     * 100 - count of product cards per batch (optional)
     *
     * @param array $params
     * @throws \Throwable
     */
    public function runAction(array $params): void
    {
        try {
            $this->parseParams($params);

            $i = 1;

            while (true) {
                $this->info('Iteration = ' . $i . ' of ' . $this->bulkTimesLimit);
                $this->info('Get batch of ProductCards by count = ' . $this->productCardBatchLimit . ' and lock them...');
                $this->reserveProductCards();
                $this->info('OK. Got ' . \count($this->productCardsLocalImagesBatch) . ' productCard(s)');

                if (!\count($this->productCardsLocalImagesBatch)) {
                    break;
                }

                $this->info('Grab images by ProductCards...');
                $this->runGrabber();
                $this->info('Grab images by ProductCards... OK');

                if ($this->bulkTimesLimit && $this->bulkTimesLimit <= $i++) {
                    break;
                }
            }

        } catch (\Exception $e) {
            if ($this->db->isUnderTransaction()) {
                $this->db->rollback();
            }

            $this->error('Catch global exception: ' . $e->getMessage());
        }
    }

    /**
     * @param array $params
     * @throws \Exception
     */
    protected function parseParams(array $params): void
    {
        if ($params && $params[0]) {
            if ('all' !== trim($params[0])) {
                $this->bulkTimesLimit = (int)$params[0];

                if (!is_numeric($params[0])) {
                    throw new \Exception('Param 1 must be integer');
                }

                $this->bulkTimesLimit = (int)$params[0];

                if (0 >= $this->bulkTimesLimit) {
                    $this->error('Bad bulk times limit in params ' . json_encode($params));
                }
            }
            if ($params[1]) {
                if (!is_numeric($params[1])) {
                    throw new \Exception('Param 2 must be integer');
                }

                $this->productCardBatchLimit = (int)$params[1];

                if (0 >= $this->productCardBatchLimit) {
                    $this->error('Bad product card batch limit in params ' . json_encode($params));
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    protected function reserveProductCards(): void
    {
        $this->db->begin();

        // Удаляем просроченные и помеченные к удалению записи
        $this->db->delete(
            'ProductCardLocalImage',
            'markToRemove IS NOT NULL OR (status = :statusNew AND createdAt < NOW() - INTERVAL ' . self::WAIT_LIMIT . ')',
            [
                'statusNew' => ProductCardLocalImage::STATUS_NEW,
            ]
        );

        // todo Временное условие. При первом наполнении важна скорость, а проблемные изображения можно пофиксить позже.
        /** @var mixed $query */
        $query = $this->db->query(
            ' SELECT pc.* FROM ProductCard pc '
//            . ' WHERE NOT EXISTS (SELECT pcli.productCardId FROM ProductCardLocalImage pcli WHERE pcli.productCardId = pc.id) '
            . ' WHERE pc.id > (SELECT MAX(pcli.productCardId) FROM ProductCardLocalImage pcli LIMIT 1) '
            . ' LIMIT :limit '
            . ' FOR UPDATE ',
            [
                'limit' => $this->productCardBatchLimit,
            ],
            [
                'limit' => Column::BIND_PARAM_INT,
            ]
        );

        if (!$query) {
            throw new \Exception('Error while trying to get unprocessed ProductCard for count = ' . $this->productCardBatchLimit);
        }

        $query->setFetchMode(Db::FETCH_CLASS, ProductCard::class);

        /** @var ProductCard[]|array $productCards */
        if (!$query || !$productCards = $query->fetchAll()) {
            throw new \Exception('Can`t get product cards by count = ' . $this->productCardBatchLimit);
        }

        $productCardsLocalImages = [];

        /** @var ProductCard $productCard*/
        foreach ($productCards as $productCard) {
            $productCardLocalImage = new ProductCardLocalImage();
            $productCardLocalImage->productCardId = $productCard->id;
            $productCardLocalImage->remoteImageUrl = $productCard->image;
            $productCardLocalImage->status = ProductCardLocalImage::STATUS_NEW;
            $productCardLocalImage->createdAt = new Db\RawValue('NOW()');

            if ($productCardLocalImage->create()) {
                $productCardsLocalImages[] = $productCardLocalImage;
            } else {
                $this->error('Can`t create ProductCardLocalImage for productCardId=' . $productCard->id);
            }
        }

        if (!$this->db->commit()) {
            throw new \Exception('Can`t commit reserve product cards');
        }

        $this->productCardsLocalImagesBatch = $productCardsLocalImages;
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    protected function runGrabber(): void
    {
        foreach ($this->productCardsLocalImagesBatch as $productCardLocalImage) {
            try {
                $remoteImageUrl = $productCardLocalImage->remoteImageUrl;

                if (!$remoteImageUrl) {
                    throw new NotExistException('ProductCard does not have image URL');
                }

                $imageSize = @getimagesize($remoteImageUrl);
                if (!$imageSize) {
                    $urlComponents = parse_url($remoteImageUrl);
                    // Если URL недоступен, пробуем поменять схему и загрузить повторно
                    $urlScheme = parse_url($remoteImageUrl, PHP_URL_SCHEME) ?? ProductCardLocalImageService::SCHEME_HTTP;
                    $remoteImageUrlWithoutScheme = preg_replace(
                        '/^(' . implode('|', ProductCardLocalImageService::URL_SCHEME_SWITCHER) . '):/i',
                        '',
                        $remoteImageUrl
                    );

                    $remoteImageUrl = ProductCardLocalImageService::URL_SCHEME_SWITCHER[$urlScheme] . ':' . $remoteImageUrlWithoutScheme;

                    $imageSize = @getimagesize($remoteImageUrl);
                    if (!$imageSize) {
                        throw new WrongExtensionException('The file is not an image');
                    }
                }

                $remoteFileContent = @file_get_contents($remoteImageUrl);
                if (!$remoteFileContent) {
                    throw new \Exception('Can`t get contents');
                }

                $localFilename = sha1($remoteFileContent) . '.' . ProductCardLocalImageService::EXTENSION_PNG;
                $localImageStatus = ProductCardLocalImage::STATUS_DONE;

                foreach (ProductCardLocalImageService::SIZES as $sizeAlias => $sizeParams) {
                    $localImageServerPath = $this->productCardLocalImageService->getLocalImageAbsolutePath($localFilename, $sizeAlias);
                    if (file_exists($localImageServerPath)) {
                        continue;
                    }

                    $img = new \Imagick();
                    if (!$img->readImageBlob($remoteFileContent)) {
                        throw new \Exception('Method Imagick::readImageBlob() is not working');
                    }
                    if (!$img->thumbnailImage($sizeParams['width'], $sizeParams['height'], true, true)) {
                        throw new \Exception('Method Imagick::thumbnailImage() is not working');
                    }
                    if (!$img->setImageFormat(ProductCardLocalImageService::EXTENSION_PNG)) {
                        throw new \Exception('Method Imagick::setImageFormat() is not working');
                    }
                    if (!$img->writeImage($localImageServerPath)) {
                        throw new \Exception('Method Imagick::writeImage() is not working');
                    }
                    if (!$img->destroy()) {
                        throw new \Exception('Method Imagick::destroy() is not working');
                    }
                }

                $productCardLocalImage->localImageFilename = $localFilename;
                $productCardLocalImage->status = $localImageStatus;
            } catch (NotExistException $e) {
                $this->warning('No local images were created for the ProductCard (id=' . $productCardLocalImage->productCardId . '). ' . $e->getMessage());
                $productCardLocalImage->status = ProductCardLocalImage::STATUS_EMPTY_SOURCE;
            } catch (WrongExtensionException $e) {
                $this->warning('No local images were created for the ProductCard (id=' . $productCardLocalImage->productCardId . '). ' . $e->getMessage());
                $productCardLocalImage->status = ProductCardLocalImage::STATUS_WRONG_EXTENSION;
            } catch (\Exception $e) {
                $this->warning('No local images were created for the ProductCard (id=' . $productCardLocalImage->productCardId . '). ' . $e->getMessage());
                $productCardLocalImage->status = ProductCardLocalImage::STATUS_NOT_FOUND;
            }

            if (!$productCardLocalImage->update()) {
                $this->error('Can`t update ProductCard data: ' . print_r($productCardLocalImage, true));
            }
        }

        $this->productCardsLocalImagesBatch = [];
    }
}
