<?php

namespace Engine\Tasks;

use Blog\Services\YouTubeParser\YouTubeParserInterface;
use Blog\Services\YouTubeParser\YouTubeParserRequestException;
use Engine\Models\ProductCard;
use Engine\Models\ProductCardYouTubeVideo;
use Engine\Models\ProductCardYouTubeVideoQuery;
use Engine\Models\ProductCardYouTubeVideoRaw;
use Engine\Models\YouTubeVideo;
use Phalcon\Db;
use Phalcon\Db\Column;

/**
 * Class YouTubeGrabberTask
 * @package Engine\Tasks
 */
class YouTubeGrabberTask extends AbstractMonitoredTask
{
    private const PRODUCT_CARD_BATCH_LIMIT = 100;

    private const WAIT_LIMIT = '3 HOUR';

    private const VIDEOS_LIMIT = 5;

    /**
     * @var int
     */
    private $batchLimit = self::PRODUCT_CARD_BATCH_LIMIT;

    /**
     * @var int
     */
    private $bulkTimesLimit;

    /**
     * @var array|ProductCardYouTubeVideoQuery[]
     */
    private $batchOfData = [];

    /**
     * @var YouTubeParserInterface
     */
    private $parserService;

    /**
     * {@inheritdoc}
     */
    public function initialize()
    {
        $this->info('Running parent::initialize()...', true);
        parent::initialize();
        $this->info('OK');

        /** @var YouTubeParserInterface $parserService */
        $this->parserService = $this->di->get('BlogYouTubeGrabberService');
    }

    /**
     * php app/cli.php YouTubeGrabberTask runStreams 4
     *
     * 4 - number of stream (required)
     *
     * @param array $params
     * @throws \Exception
     */
    public function runStreamsAction(array $params): void
    {
        if ('dev' === APP_ENV) {
            if (!isset($params[0]) || !is_numeric($params[0]) || $params[0] < 1) {
                throw new \Exception('Wrong params');
            }

            $cnt = (int)$params[0];

            for ($i = 0; $i < $cnt; $i++) {
                $this->info('Run process #' . $i . '...');
//                exec('php app/cli.php YouTubeGrabber run 1 10 > /dev/null 2>&1 &');
//                exec('php app/cli.php YouTubeGrabber run 1 10');
            }
        }
    }

    /**
     * php app/cli.php YouTubeGrabber run
     * php app/cli.php YouTubeGrabber run 1
     * php app/cli.php YouTubeGrabber run 1 10
     * php app/cli.php YouTubeGrabber run all 100
     *
     * 1 - count of batches (optional)
     * 100 - count of product cards per batch (optional)
     *
     * @param array $params
     */
    public function runAction(array $params): void
    {
        try {
            $this->parseParams($params);

            $i = 1;

            while (true) {
                $this->info('Iteration = ' . $i . ' of ' . $this->bulkTimesLimit);
                $this->info('Get batch of items by count = ' . $this->batchLimit . ' and lock them...');
                $this->reserve();
                $this->info('OK. Got ' . \count($this->batchOfData) . ' item(s)');

                $this->info('Garbage collection...');
                $this->garbageCollection();
                $this->info('Garbage collection... OK');

                if (!\count($this->batchOfData)) {
                    break;
                }

                $this->info('Grab items...');
                $this->grab();
                $this->info('Grab items... OK');

                $this->info('Structure items...');
                $this->structure();
                $this->info('Structure items... OK');

                if ($this->bulkTimesLimit && $this->bulkTimesLimit <= $i++) {
                    break;
                }

                sleep(1);
            }

        } catch (\Exception $ex) {
            if ($this->db->isUnderTransaction()) {
                $this->db->rollback();
            }

            $this->error($ex->getMessage());
        }
    }

    /**
     * @param array $params
     * @throws \Exception
     */
    protected function parseParams(array $params): void
    {
        if ($params && $params[0]) {
            if ('all' !== trim($params[0])) {
                $this->bulkTimesLimit = (int)$params[0];

                if (!is_numeric($params[0])) {
                    throw new \Exception('Param 1 must be integer');
                }

                $this->bulkTimesLimit = (int)$params[0];

                if (0 >= $this->bulkTimesLimit) {
                    $this->error('Bad bulk times limit in params ' . json_encode($params));
                }
            }
            if ($params[1]) {
                if (!is_numeric($params[1])) {
                    throw new \Exception('Param 2 must be integer');
                }

                $this->batchLimit = (int)$params[1];

                if (0 >= $this->batchLimit) {
                    $this->error('Bad batch limit in params ' . json_encode($params));
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function garbageCollection(): void
    {
        $this->db->begin();

        // Удаляем просроченные raw-записи
        $this->db->delete(
            'ProductCardYouTubeVideoRaw',
            ' productCardId IN ('
            . ' SELECT productCardId FROM ProductCardYouTubeVideoQuery'
            . ' WHERE (status = :statusNew OR status = :statusProgress)'
            . ' AND createdAt < NOW() - INTERVAL ' . self::WAIT_LIMIT
            . ' )',
            [
                'statusNew' => ProductCardYouTubeVideoQuery::STATUS_NEW,
                'statusProgress' => ProductCardYouTubeVideoQuery::STATUS_PROGRESS,
            ]
        );
        // Удаляем просроченные query-записи
        $this->db->delete(
            'ProductCardYouTubeVideoQuery',
            '(status = :statusNew OR status = :statusProgress) AND createdAt < NOW() - INTERVAL ' . self::WAIT_LIMIT,
            [
                'statusNew' => ProductCardYouTubeVideoQuery::STATUS_NEW,
                'statusProgress' => ProductCardYouTubeVideoQuery::STATUS_PROGRESS,
            ]
        );

        if (!$this->db->commit()) {
            throw new \Exception('Can`t commit garbageCollection'
                . __CLASS__ . '::' . __METHOD__);
        }
    }

    /**
     * @throws \Exception
     */
    protected function reserve(): void
    {
        $this->db->begin();

        /** @var mixed $query */
        $query = $this->db->query(
            ' SELECT pc.* FROM ProductCard pc '
            . ' WHERE NOT EXISTS (SELECT q.productCardId FROM ProductCardYouTubeVideoQuery q WHERE q.productCardId = pc.id) '
            . ' LIMIT :limit '
            . ' FOR UPDATE ',
            [
                'limit' => $this->batchLimit,
            ],
            [
                'limit' => Column::BIND_PARAM_INT,
            ]
        );

        if (!$query) {
            throw new \Exception('Error while trying to get unprocessed ProductCard for count = ' . $this->batchLimit . '. '
                . __CLASS__ . '::' . __METHOD__);
        }

        $query->setFetchMode(Db::FETCH_CLASS, ProductCard::class);

        /** @var ProductCard[]|array $productCards */
        if (!$query || !$productCards = $query->fetchAll()) {
            throw new \Exception('Cant get ProductCard by count = ' . $this->batchLimit . '. '
                . __CLASS__ . '::' . __METHOD__);
        }

        $productCardYouTubeVideoQueries = [];

        /** @var ProductCard $productCard*/
        foreach ($productCards as $productCard) {
            $productCardYouTubeVideoQuery = new ProductCardYouTubeVideoQuery();
            $productCardYouTubeVideoQuery->productCardId = $productCard->id;
            $productCardYouTubeVideoQuery->query = json_encode([
                $productCard->title,
                $productCard->getCategory()->title . ' ' . $productCard->getBrand()->title,
            ]);
            $productCardYouTubeVideoQuery->status = ProductCardYouTubeVideoQuery::STATUS_NEW;
            $productCardYouTubeVideoQuery->createdAt = new Db\RawValue('NOW()');

            if (!$productCardYouTubeVideoQuery->create()) {
                // todo добавить логирование
            } else {
                $productCardYouTubeVideoQueries[] = $productCardYouTubeVideoQuery;
            }
        }

        if (!$this->db->commit()) {
            throw new \Exception('Can`t commit reserve product cards'
                . __CLASS__ . '::' . __METHOD__);
        }

        $this->batchOfData = $productCardYouTubeVideoQueries;
    }

    /**
     * @throws \Exception
     */
    private function grab(): void
    {
        // todo Добавить удаление устаревших ProductCardYouTubeVideoRaw со статусом !DONE ()

        /** @var ProductCardYouTubeVideoQuery $productCardYouTubeVideoQuery */
        foreach ($this->batchOfData as $productCardYouTubeVideoQuery) {
            try {
                $query = $productCardYouTubeVideoQuery->query;
                $queriesList = json_decode($query, true);
                $videosCount = 0;

                foreach ((array)$queriesList as $queryString) {
                    $youTubeVideosData = $this->parserService->requestVideos(
                        $queryString,
                        self::VIDEOS_LIMIT
                    );

                    if (!empty($youTubeVideosData)) {
                        foreach ($youTubeVideosData as $youTubeVideoData) {
                            $productCardYouTubeVideoRaw = new ProductCardYouTubeVideoRaw();
                            $productCardYouTubeVideoRaw->productCardId = $productCardYouTubeVideoQuery->productCardId;
                            $productCardYouTubeVideoRaw->createdAt = new Db\RawValue('NOW()');
                            if ($productCardYouTubeVideoRaw->create($youTubeVideoData, ProductCardYouTubeVideoRaw::EDITABLE_WHITELIST)) {
                                $videosCount++;
                            } else {
                                $this->warning('Не удалось добавить запись ProductCardYouTubeVideoRaw'
                                    . __CLASS__ . '::' . __METHOD__);
                            }
                        }

                        break;
                    }
                }

                $productCardYouTubeVideoQuery->status = $videosCount
                    ? ProductCardYouTubeVideoQuery::STATUS_PROGRESS
                    : ProductCardYouTubeVideoQuery::STATUS_NOT_FOUND;

            } catch (YouTubeParserRequestException $e) {
                $productCardYouTubeVideoQuery->status = ProductCardYouTubeVideoQuery::STATUS_ACCESS_DENIED;
                throw $e;
            } catch (\Exception $e) {
                // Это событие сейчас не инициируется. Добавлено на всякий случай.
                $productCardYouTubeVideoQuery->status = ProductCardYouTubeVideoQuery::STATUS_ERROR;
                throw $e;
            }

            if (!$productCardYouTubeVideoQuery->update()) {
                $this->error('Не удалось обновить статус записи ProductCardYouTubeVideoQuery. '
                    . __CLASS__ . '::' . __METHOD__);
            }
        }
    }

    /**
     * @throws \Exception
     */
    private function structure(): void
    {
        // Получаем ID всех товаров за текущую итерацию
        $bulkProductCardsIds = [];
        foreach ($this->batchOfData as $productCardYouTubeVideoQuery) {
            $bulkProductCardsIds[] = $productCardYouTubeVideoQuery->productCardId;
        }

        // Удаляем старые связи товаров с видео по товарам текущей итерации.
        // Есть большая доля вероятности, что этот код не будет срабатывать,
        // т.к. добавление в эту таблицу происходит в транзакции. Но все равно, пусть будет.
        $this->db->delete(
            'ProductCardYouTubeVideo',
            'productCardId IN (' . \implode(',', array_map([$this->db, 'escapeString'], $bulkProductCardsIds)) . ') '
        );

        $this->db->begin();

        // Запрашиваем сырые данные о видео по этим товарам (по каждому товару должно быть хотя бы одно видео)
        /** @var mixed $query */
        $query = $this->db->query(
            'SELECT r.* FROM ProductCardYouTubeVideoRaw r '
            . 'JOIN ProductCardYouTubeVideoQuery q USING (productCardId) '
            . 'WHERE r.productCardId IN (' . \implode(',', array_map([$this->db, 'escapeString'], $bulkProductCardsIds)) . ') '
            . 'AND q.`status` = :statusProgress '
            . 'LIMIT :limit '
            . 'FOR UPDATE ',
            [
                'statusProgress' => ProductCardYouTubeVideoQuery::STATUS_PROGRESS,
                'limit' => $this->batchLimit * self::VIDEOS_LIMIT,
            ],
            [
                'limit' => Column::BIND_PARAM_INT,
            ]
        );

        if (!$query) {
            throw new \Exception('Error while trying to get unprocessed ProductCardYouTubeVideoRaw items for count = ' . $this->batchLimit . '. '
                . __CLASS__ . '::' . __METHOD__);
        }

        $query->setFetchMode(Db::FETCH_CLASS, ProductCardYouTubeVideoRaw::class);

        if (!$query) {
            throw new \Exception('Cant get ProductCardYouTubeVideoRaw items for count = ' . $this->batchLimit . '. '
                . __CLASS__ . '::' . __METHOD__);
        }

        /** @var ProductCardYouTubeVideoRaw[]|array $productCardYouTubeVideoRaws */
        $productCardYouTubeVideoRaws = $query->fetchAll();
        if ($productCardYouTubeVideoRaws) {
            // Создаем новые связи товаров с видео на основании сырых данных
            foreach ($productCardYouTubeVideoRaws as $productCardYouTubeVideoRaw) {
                $youTubeVideo = YouTubeVideo::findFirstByColumn('videoKey', $productCardYouTubeVideoRaw->videoKey);

                if (!$youTubeVideo) {
                    $youTubeVideo = new YouTubeVideo();
                    $youTubeVideo->createdAt = new Db\RawValue('NOW()');
                    if (!$youTubeVideo->create($productCardYouTubeVideoRaw->toArray(), YouTubeVideo::EDITABLE_WHITELIST)) {
                        throw new \Exception('Не удалось добавить запись о видео. '
                            . __CLASS__ . '::' . __METHOD__);
                    }
                }

                $productCardYouTubeVideo = new ProductCardYouTubeVideo();
                $productCardYouTubeVideo->productCardId = $productCardYouTubeVideoRaw->productCardId;
                $productCardYouTubeVideo->youTubeVideoId = $youTubeVideo->id;
                $productCardYouTubeVideo->createdAt = new Db\RawValue('NOW()');
                if (!$productCardYouTubeVideo->create()) {
                    throw new \Exception('Не удалось создать связь товара с видео. '
                        . __CLASS__ . '::' . __METHOD__);
                }
            }

            // Удаляем записи сырых данных после успершной привязки
            $this->db->delete(
                'ProductCardYouTubeVideoRaw',
                'productCardId IN (' . \implode(',', array_map([$this->db, 'escapeString'], $bulkProductCardsIds)) . ') '
            );
        } else {
            $this->warning('Не найдено ни одной ProductCardYouTubeVideoRaw записи. Так быть не должно! '
                . __CLASS__ . '::' . __METHOD__);
        }

        // Проставляем статусы для всех запросов об успешно завершенной операции
        foreach ($this->batchOfData as $productCardYouTubeVideoQuery) {
            $productCardYouTubeVideoQuery->status = ProductCardYouTubeVideoQuery::STATUS_DONE;
            if (!$productCardYouTubeVideoQuery->update()) {
                throw new \Exception('Не удалось поставить завершающий статус ProductCardYouTubeVideoQuery. '
                    . __CLASS__ . '::' . __METHOD__);
            }
        }

        if (!$this->db->commit()) {
            throw new \Exception('Can`t structure product cards youTube videos. '
                . __CLASS__ . '::' . __METHOD__);
        }
    }
}
