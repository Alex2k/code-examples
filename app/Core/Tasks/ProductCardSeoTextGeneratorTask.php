<?php

namespace Engine\Tasks;

use Engine\Exceptions\DataException;
use Engine\Exceptions\ObjectNotFoundException;
use Engine\Models\Category;
use Engine\Models\ProductCard;
use Engine\Models\ProductCardSeoTemplate;
use Engine\Models\ProductCardSeoTemplateItem;
use Engine\Models\SeoTemplate;
use Engine\Services\SeoContentGeneratorService;
use Phalcon\Db;
use Phalcon\Db\Column;

/**
 * Class ProductCardSeoTextGeneratorTask
 * @package Engine\Tasks
 */
class ProductCardSeoTextGeneratorTask extends AbstractTask
{
    public const WAIT_LIMIT = '3 HOUR';

    /**
     * @var int
     */
    private $productCardBatchLimit = 100;

    /**
     * @var int
     */
    private $bulkTimesLimit;

    /**
     * @var ProductCardSeoTemplate[]|null
     */
    private $productCardsSeoTemplatesBatch = [];

    /**
     * {@inheritdoc}
     */
    public function initialize(): void
    {
        $this->info('Running parent::initialize()...', true);
        parent::initialize();
        $this->info('OK');
    }

    /**
     * php app/cli.php ProductCardSeoTextGenerator run
     * php app/cli.php ProductCardSeoTextGenerator run 1 10
     *
     * 1 - count of batches (optional)
     * 10 - count of product cards per batch (optional)
     *
     * @param array $params
     * @throws \Throwable
     */
    public function runAction(array $params): void
    {
        try {
            $this->parseParams($params);

            $i = 1;

            while (true) {
                $this->info('Iteration = ' . $i . ' of ' . $this->bulkTimesLimit);
                $this->info('Get batch of ProductCards by count = ' . $this->productCardBatchLimit . ' and lock them...');
                $this->reserveProductCards();
                $this->info('OK. Got ' . \count($this->productCardsSeoTemplatesBatch) . ' productCard(s)');

                if (!\count($this->productCardsSeoTemplatesBatch)) {
                    break;
                }

                $this->info('Fill templates by ProductCards...');
                $this->runTemplater();
                $this->info('Fill templates by ProductCards... OK');

                if ($this->bulkTimesLimit && $this->bulkTimesLimit <= $i++) {
                    break;
                }
            }

        } catch (\Exception $e) {
            if ($this->db->isUnderTransaction()) {
                $this->db->rollback();
            }

            $this->error('Catch global exception: ' . $e->getMessage());
        }
    }

    /**
     * @param array $params
     * @throws \Exception
     */
    protected function parseParams(array $params): void
    {
        if ($params && $params[0]) {
            if ('all' !== trim($params[0])) {
                $this->bulkTimesLimit = (int)$params[0];

                if (!is_numeric($params[0])) {
                    throw new \Exception('Param 1 must be integer');
                }

                $this->bulkTimesLimit = (int)$params[0];

                if (0 >= $this->bulkTimesLimit) {
                    $this->error('Bad bulk times limit in params ' . json_encode($params));
                }
            }
            if ($params[1]) {
                if (!is_numeric($params[1])) {
                    throw new \Exception('Param 2 must be integer');
                }

                $this->productCardBatchLimit = (int)$params[1];

                if (0 >= $this->productCardBatchLimit) {
                    $this->error('Bad product card batch limit in params ' . json_encode($params));
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    protected function reserveProductCards(): void
    {
        // Удаляем просроченные записи
        $this->db->delete(
            'ProductCardSeoTemplate',
            'status = :statusNew AND createdAt < NOW() - INTERVAL ' . self::WAIT_LIMIT,
            [
                'statusNew' => ProductCardSeoTemplate::STATUS_NEW,
            ]
        );

        $this->db->begin();

        /** @var mixed $query */
        $query = $this->db->query(
            ' SELECT pc.* FROM ProductCard pc '
            . ' WHERE EXISTS (SELECT st.categoryId FROM SeoTemplate st WHERE st.categoryId = pc.categoryId) '
            . ' AND NOT EXISTS (SELECT pcst.productCardId FROM ProductCardSeoTemplate pcst WHERE pcst.productCardId = pc.id) '
            . ' LIMIT :limit '
            . ' FOR UPDATE ',
            [
                'limit' => $this->productCardBatchLimit,
            ],
            [
                'limit' => Column::BIND_PARAM_INT,
            ]
        );

        if (!$query) {
            throw new \Exception('Error while trying to get unprocessed ProductCard for count = ' . $this->productCardBatchLimit);
        }

        $query->setFetchMode(Db::FETCH_CLASS, ProductCard::class);

        if (!$query) {
            throw new \Exception('Can`t get product cards by count = ' . $this->productCardBatchLimit);
        }

        $productCardsSeoTemplates = [];

        /** @var ProductCard[]|array $productCards */
        if ($productCards = $query->fetchAll()) {
            /** @var ProductCard $productCard*/
            foreach ($productCards as $productCard) {
                $productCardsSeoTemplate = new ProductCardSeoTemplate();
                $productCardsSeoTemplate->productCardId = $productCard->id;
                $productCardsSeoTemplate->categoryId = $productCard->categoryId;
                $productCardsSeoTemplate->status = ProductCardSeoTemplate::STATUS_NEW;
                $productCardsSeoTemplate->createdAt = new Db\RawValue('NOW()');

                if ($productCardsSeoTemplate->create()) {
                    $productCardsSeoTemplates[] = $productCardsSeoTemplate;
                } else {
                    $this->error('Can`t create ProductCardSeoTemplate for productCardId=' . $productCard->id);
                }
            }
        }

        if (!$this->db->commit()) {
            throw new \Exception('Can`t commit reserve product cards');
        }

        $this->productCardsSeoTemplatesBatch = $productCardsSeoTemplates;
    }

    protected function runTemplater(): void
    {
        foreach ($this->productCardsSeoTemplatesBatch as $productCardSeoTemplate) {
            try {
                $this->generateTemplate($productCardSeoTemplate);
                $productCardSeoTemplate->status = ProductCardSeoTemplate::STATUS_DONE;
            } catch (\Exception $e) {
                $this->warning('No seo description template were created for the ProductCard (id=' . $productCardSeoTemplate->productCardId . '). ' . $e->getMessage());
                $productCardSeoTemplate->status = ProductCardSeoTemplate::STATUS_FAILURE;
            }

            if (!$productCardSeoTemplate->update()) {
                $this->error('Can`t update ProductCard data: ' . print_r($productCardSeoTemplate->toArray(), true));
            }
        }

        $this->productCardsSeoTemplatesBatch = [];
    }

    /**
     * @param ProductCardSeoTemplate $productCardSeoTemplate
     * @throws DataException
     * @throws ObjectNotFoundException
     */
    protected function generateTemplate(ProductCardSeoTemplate $productCardSeoTemplate): void
    {
        // Удаляем просроченные записи
        $this->db->delete(
            'ProductCardSeoTemplateItem',
            'productCardSeoTemplateId = :productCardSeoTemplateId',
            [
                'productCardSeoTemplateId' => $productCardSeoTemplate->id,
            ]
        );

        $seoTemplates = SeoTemplate::findByCategoryId($productCardSeoTemplate->categoryId);

        if (!$seoTemplates->count()) {
            throw new ObjectNotFoundException();
        }

        $seoTemplatesData = [];

        foreach ($seoTemplates as $seoTemplate) {
            if (!isset($seoTemplatesData[$seoTemplate->index])) {
                $seoTemplatesData[$seoTemplate->index] = [];
            }

            $seoTemplatesData[$seoTemplate->index][$seoTemplate->version] = $seoTemplate->id;
        }

        if (!ksort($seoTemplatesData)) {
            throw new DataException();
        }

        foreach ($seoTemplatesData as $seoTemplateData) {
            $productCardSeoTemplateItem = new ProductCardSeoTemplateItem();
            $productCardSeoTemplateItem->productCardSeoTemplateId = $productCardSeoTemplate->id;

            $randKey = array_rand($seoTemplateData, 1);
            $productCardSeoTemplateItem->seoTemplateId = $seoTemplateData[$randKey];

            if (!$productCardSeoTemplateItem->create()) {
                throw new DataException('Relation is not created');
            }
        }
    }
}
